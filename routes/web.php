<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'TwitterController@listUsers')->name('list_users');
Route::get('/search', 'TwitterController@searchTweets')->name('search_tweets');
Route::get('/{username}', 'TwitterController@showUser')->name('show_user');
