<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List User View</title>
</head>
<body>
<h2>Search:</h2>

{{ Form::open(['method' => 'get']) }}
{{ Form::text('query') }}
{{ Form::select('user', $twitter_users) }}
{{ Form::submit() }}
{{ Form::close() }}

<ul>
    @forelse($tweets as $weeet)
        <li>{{ $weeet->text }}</li>
    @empty
        <li>No results</li>
    @endforelse
</ul>
{{ $tweets->appends(['query' => request('query'), 'user' => request('user')])->links() }}
</body>
</html>
