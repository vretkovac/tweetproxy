<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List User View</title>
</head>
<body>
<h2>Users:</h2>
<ul>
    @foreach($twitter_users as $twitter_user)
        <li><a href="{{ route('show_user', $twitter_user->screen_name ) }}">{{ $twitter_user->screen_name }}</a></li>
    @endforeach
</ul>
</body>
</html>
