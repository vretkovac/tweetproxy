<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Single User View</title>
</head>
<body>
<h2>User: {{ $twitter_user->screen_name }}</h2>
<ul>
    @foreach($tweets as $tweet)
        <li>{{$tweet->text}}</li>
    @endforeach
</ul>
</body>
</html>
