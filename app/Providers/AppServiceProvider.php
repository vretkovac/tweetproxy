<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Domain\TwitterApi;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('TwitterApi', function ($app) {
            return new TwitterApi();
        });
    }
}
