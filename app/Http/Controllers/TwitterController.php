<?php

namespace App\Http\Controllers;

use App\Domain\TwitterProxy;
use App\Models\Tweet;
use App\Models\TwitterUser;
use Illuminate\Http\Request;

class TwitterController extends Controller
{
    public function listUsers()
    {
        $twitter_users = TwitterUser::all();

        return view('users.list', [
            'twitter_users' => $twitter_users,
        ]);
    }

    public function showUser($screen_name)
    {
        /** @var TwitterUser $twitter_user */
        $twitter_user = TwitterProxy::getUser($screen_name);

        if (!$twitter_user) {
            abort(400, 'Twitter user does not exists.');
        }

        return view('users.show', [
            'twitter_user' => $twitter_user,
            'tweets'       => $twitter_user->tweets,
        ]);
    }

    public function searchTweets(Request $request)
    {
        $tweets = Tweet::search($request->get('query'))
                       ->user($request->get('user'))
                       ->paginate(config('twitter.tweets_per_page'));

        $twitter_users = TwitterUser::all()
                                    ->prepend((object)['id' => 0, 'screen_name' => 'All users'])// Add all users default
                                    ->mapWithKeys(function ($item) {
                return [$item->id => $item->screen_name];
            });

        return view('tweets.search', [
            'twitter_users' => $twitter_users->toArray(),
            'tweets'        => $tweets,
        ]);
    }
}
