<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $table = 'tweets';

    protected $fillable = [
        'twitter_user_local_id',
        'tweet_id',
        'text',
        'posted_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'posted_at',
    ];

    /* SCOPES */
    public function scopeUser(Builder $query, $user_id)
    {
        if (!$user_id){
            return $query;
        }

        return $query->where('twitter_user_local_id', '=', $user_id);
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        return $query->whereRaw('MATCH (text) AGAINST (?)', [$q]);
    }
}
