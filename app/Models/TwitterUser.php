<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterUser extends Model
{
    protected $table = 'twitter_users';

    protected $fillable = [
        'screen_name',
    ];

    /* RELATIONS */
    public function tweets()
    {
        return $this->hasMany(Tweet::class, 'twitter_user_local_id');
    }

    /* SCOPES */
    public function scopeScreenName($query, $screen_name)
    {
        return $query->where('screen_name', $screen_name);
    }
}
