<?php

namespace App\Domain;

use App\Models\TwitterUser;
use Carbon\Carbon;

class TwitterProxy
{
    /**
     * @param string $screen_name
     * @return TwitterUser
     */
    public static function getUser($screen_name)
    {
        $twitter_user = TwitterUser::screenName($screen_name)->first();

        // Twitter user already in databse
        if ($twitter_user) {
            return $twitter_user;
        }

        // Twitter user doesn't exist
        if (!self::fetchUser($screen_name)) {
            return null;
        }

        // Save new twitter user in database
        $twitter_user = TwitterUser::create([
            'screen_name' => $screen_name,
        ]);

        // Save user latest tweets
        $tweets = self::fetchTweets($screen_name);
        $twitter_user->tweets()->createMany($tweets->toArray());

        return $twitter_user;
    }

    /**
     * @param string $screen_name
     * @return mixed|null
     */
    protected static function fetchUser($screen_name)
    {
        $api = resolve('TwitterApi');

        $result = $api->users_search($screen_name);

        return collect($result)->where('screen_name', $screen_name)->first();
    }

    /**
     * @param string $screen_name
     * @return \Collection
     */
    protected static function fetchTweets($screen_name)
    {
        $api = resolve('TwitterApi');

        $result = $api->statuses_user_timeline($screen_name);

        return collect($result)->map(function ($item) {
            return [
                'tweet_id'  => $item->id_str,
                'text'      => $item->text,
                'posted_at' => Carbon::parse($item->created_at),
            ];
        });
    }
}
