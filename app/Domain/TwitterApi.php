<?php

namespace App\Domain;

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterApi
{
    protected $api;
    protected $consumer_key;
    protected $consumer_secret;
    protected $access_token;
    protected $access_token_secret;

    public function __construct()
    {
        $this->consumer_key = config('twitter.consumer_key');
        $this->consumer_secret = config('twitter.consumer_secret');
        $this->access_token = config('twitter.acces_token');
        $this->access_token_secret = config('twitter.access_token_secret');

        $this->api = new TwitterOAuth($this->consumer_key, $this->consumer_secret, $this->access_token, $this->access_token_secret);
    }

    /**
     * Test if Twitter connection is OK
     *
     * @return mixed
     */
    public function verify_credentials()
    {
        return $this->api->get('account/verify_credentials');
    }

    /**
     * Search Twitter users by screen_name
     *
     * @param string $q
     * @return mixed
     */
    public function users_search($q)
    {
        $params = [
            'q'     => $q,
            'page'  => 1,
            'count' => 10,
        ];

        return $this->api->get('users/search', $params);
    }

    /**
     * Returns most recent user tweets
     *
     * @param $screen_name
     * @return mixed
     */
    public function statuses_user_timeline($screen_name)
    {
        $params = [
            'screen_name' => $screen_name,
            'count'       => 20,
        ];

        return $this->api->get('statuses/user_timeline', $params);
    }
}
